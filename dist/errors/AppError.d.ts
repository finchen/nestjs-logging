/**
 * usage: throw new AppError("Describe here what happened", true);
 */
export declare class AppError extends Error {
    /**
     * Class name in the property of our custom error as a shortcut
     */
    name: string;
    /**
     * Operational errors (e.g. API received an invalid input) refer to known cases where the error impact is fully understood and can be handled thoughtfully.
     * Just logging info
     */
    isOperational: boolean;
    /**
     * System errors (e.g. HTTP Timeout or Database insert failed) refer to error that should not happen.
     * Error Logging + warning
     */
    isSystem: boolean;
    /**
     * Critical errors (e.g. Can not write a file that should have read access) refer to error that corrupt the application state and require a restart
     * Error Logging + alert
     */
    isCritical: boolean;
    /**
     *
     * @param message
     * @param isOperational `true` known cases, `false` unknown code failures
     */
    constructor(message: string, errorLevel?: null | 'operational' | 'system' | 'critical');
    getResponse(): {
        status: number;
        name: string;
        message: string;
        isOperational: boolean;
    };
}
