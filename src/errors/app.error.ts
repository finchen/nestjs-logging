/**
 * usage: throw new AppError("Describe here what happened", true);
 */
export class AppError extends Error {
  /**
   * Class name in the property of our custom error as a shortcut
   */
  name: string;
  /**
   * Operational errors (e.g. API received an invalid input) refer to known cases where the error impact is fully understood and can be handled thoughtfully.
   * Just logging info
   */
  isOperational: boolean;
  /**
   * System errors (e.g. HTTP Timeout or Database insert failed) refer to error that should not happen.
   * Error Logging + warning
   */
  isSystem: boolean;
  /**
   * Critical errors (e.g. Can not write a file that should have read access) refer to error that corrupt the application state and require a restart
   * Error Logging + alert
   */
  isCritical: boolean;

  // All other unhandledError (e.g. trying to read undefined variable) refers to unknown code failures. Error Logging + alert

  /**
   *
   * @param message
   * @param isOperational `true` known cases, `false` unknown code failures
   */
  constructor(
    message: string,
    errorLevel?: null | 'operational' | 'system' | 'critical'
  ) {
    // Calling parent constructor of base Error class.
    super(message);

    this.name = this.constructor.name;
    this.isOperational = errorLevel !== null && errorLevel === 'operational';
    this.isSystem = errorLevel !== null && errorLevel === 'system';
    this.isCritical = errorLevel !== null && errorLevel === 'critical';

    // Capturing stack trace, excluding constructor call from it.
    Error.captureStackTrace(this, this.constructor);
  }

  getResponse() {
    return {
      status: 500,
      name: this.name,
      message: this.message,
      isOperational: this.isOperational,
    };
  }
}
