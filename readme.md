> * This is a module for Nest.js written during my position at Eurofins
> * It is part of a Nest.js boilerplate 
> * This module helped normalizing the logging in development and production for different projects

A Nest module wrapper for winston logger.

## Eurofins

This is a fork of [nest-winston](https://www.npmjs.com/package/nest-winston).

It includes:
 >*  winston logger preconfigured
 >*  [pretty](https://www.npmjs.com/package/pretty-error) console error for development
 >*  collection of custom exceptions/error
 >*  catch exception and do the right logging according to th etype of error

## Installation / Usage

Include it in the project you want to use
```bash
npm install --save git+https://bitbucket.api.agfirstbop.co.nz/scm/ntr/nestjs-logging.git
```

If you have credential issue, please [generate or find](https://confluence.atlassian.com/bitbucketserver051/creating-ssh-keys-928678744.html) your rsa key to a bitbucket admin. Admin will add your key in the settings of the repository. You can then pull the repo without a username/password.

Fall back. Unsafe as the password will be saved in the package.json

```bash
npm install --save git+https://bitbucket.api.agfirstbop.co.nz/scm/ntr/nestjs-logging.git
```



## Quick Start

Import `WinstonModule` into the root `AppModule` and use the `forRoot()` method to configure it. This method accepts the same options object as [`createLogger()`](https://github.com/winstonjs/winston#usage) function from the winston package:

```typescript
import { Module } from '@nestjs/common';
import { WinstonModule } from 'nest-winston';
import * as winston from 'winston';

@Module({
  imports: [
    WinstonModule.forRoot(),
  ],
  providers: [
    ErrorsInterceptor
  ]
})
export class AppModule {}
```

Module is preconfigured in ./src/logger/default-winston-options.ts but you can override the options with
```typescript
@Module({
  imports: [
    WinstonModule.forRoot({
      // options
    }),
  ],
  providers: [
    ErrorsInterceptor
  ]
})
export class AppModule {}
```
If you need to override, please ask us why and we will try to improve this package first.


Afterward, the winston instance will be available to inject across entire project using the `winston` injection token:

```typescript
import { Controller, Inject } from '@nestjs/common';
import { Logger } from 'winston';

@Controller('cats')
export class CatsController {
  constructor(@Inject('winston') private readonly logger: Logger) { }
}
```

Note that `WinstonModule` is a global module, it will be available in all you feature modules.



## Async configuration

> **Caveats**: because the way Nest works, you can't inject dependencies exported from the root module itself (using `exports`). If you use `forRootAsync()` and need to inject a service, that service must be either imported using the `imports` options or exported from a [global module](https://docs.nestjs.com/modules#global-modules).

Maybe you need to asynchronously pass your module options, for example when you need a configuration service. In such case, use the `forRootAsync()` method, returning an options object from the `useFactory` method:

```typescript
import { Module } from '@nestjs/common';
import { WinstonModule } from 'nest-winston';
import * as winston from 'winston';

@Module({
  imports: [
    WinstonModule.forRootAsync({
      useFactory: () => ({
        // options
      }),
      inject: [],
      providers: [
        ErrorsInterceptor
      ]
    }),
  ],
})
export class AppModule {}
```

The factory might be async, can inject dependencies with `inject` option and import other modules using the `imports` option.

## Exception

Avoid using generic exception.

Really bad:
```typescript
throw "an error"
```

Bad:
```typescript
throw new Error("an error")
```

Better
```typescript
throw new AppError("an error", 'operational' | 'system' | 'critical')
```
[Error types](./src/errors/AppError.ts)

## Interceptor

Error are caught by the interceptors which will decide how to log in file, console or dispatch

## How to develop this package?

- clone the repo
- work on it
- compile `npm run build`
- you can use `npm link` to import it locally in a project
  -  `npm link` the package
  -  import it with `npm link @eurofins/nestjs-logging`