import { Provider } from '@nestjs/common';
import { WinstonModuleAsyncOptions, WinstonModuleOptions } from './winston.interfaces';
export declare function createWinstonProviders(loggerOpts: WinstonModuleOptions): Provider[];
export declare function createWinstonAsyncProviders(options: WinstonModuleAsyncOptions): Provider[];
