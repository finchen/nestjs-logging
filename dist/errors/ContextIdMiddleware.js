"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const uuid_1 = require("uuid");
const httpContext = require("express-http-context");
let ContextIdMiddleware = class ContextIdMiddleware {
    constructor() {
        this.headerName = 'X-RequestId';
    }
    use(req, res, next) {
        // use a complete random uuid; v4
        const uuid = uuid_1.v4();
        httpContext.set('reqId', uuid);
        // Set it in header so it can be easily retrieved from client
        res.setHeader(this.headerName, uuid);
        next();
    }
};
ContextIdMiddleware = __decorate([
    common_1.Injectable()
], ContextIdMiddleware);
exports.ContextIdMiddleware = ContextIdMiddleware;
//# sourceMappingURL=ContextIdMiddleware.js.map