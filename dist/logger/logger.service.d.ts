import { LoggerOptions } from 'winston';
import { LoggerService } from '@nestjs/common';
import { WinstonModuleOptions } from '../winston.interfaces';
export declare class CustomLoggerService implements LoggerService {
    private readonly config;
    private readonly logger;
    private prettyError;
    /**
     * global/app context
     * does not apply for class or filename
     * default `Main`
     */
    private context;
    constructor(config: WinstonModuleOptions);
    setContext(c: string): void;
    getMetas(context?: string): {
        timestamp: string;
        context: string;
        requestId: any;
    };
    log(message: string): void;
    error(message: string | object, trace?: string, context?: string): void;
    warn(message: string): void;
    debug(message: string): void;
    verbose(message: string): void;
    overrideOptions(options: LoggerOptions): void;
    private formatedLog;
    private printMessage;
}
