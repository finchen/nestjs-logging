import { AppError } from './AppError';
export declare class EmailTakenError extends AppError {
    constructor(message: string);
}
