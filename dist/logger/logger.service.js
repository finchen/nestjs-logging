"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const winston = require("winston");
const chalk = require("chalk");
const PrettyError = require("pretty-error");
const common_1 = require("@nestjs/common");
const httpContext = require("express-http-context");
let CustomLoggerService = 
/**
 * Log
 * - to winston
 * - on console when on dev
 * - error on console printed with [pretty-error]()
 * - add the requestId of [express-http-context]() in the log file (not on console as it is probably useless or maybe just for prod console?)
 */
class CustomLoggerService {
    constructor(config) {
        this.config = config;
        this.logger = winston.createLogger(config);
        this.prettyError = new PrettyError();
        this.prettyError.skipNodeFiles();
        //this.prettyError.skipPackage('express', '@nestjs/common', '@nestjs/core');
        this.context = this.config.context ? this.config.context : 'Main';
    }
    setContext(c) {
        this.context = c;
    }
    getMetas(context) {
        const currentDate = new Date();
        const extras = {
            timestamp: currentDate.toISOString(),
            context: context ? context : this.context,
            requestId: httpContext.get('reqId'),
        };
        return extras;
    }
    log(message) {
        this.logger.info(message, this.getMetas());
        this.formatedLog('info', message);
    }
    error(message, trace, context) {
        if (typeof message === 'string') {
            this.logger.error(`${message} -> (${trace || 'trace not provided !'})`, this.getMetas(context));
        }
        else {
            this.logger.error(`Error -> (${trace || 'trace not provided !'})`, Object.assign({}, message, this.getMetas(context)));
        }
        this.formatedLog('error', message, trace, context);
    }
    warn(message) {
        this.logger.warn(message, this.getMetas());
        this.formatedLog('warn', message);
    }
    debug(message) {
        this.logger.debug(message, this.getMetas());
        this.formatedLog('debug', message);
    }
    verbose(message) {
        this.logger.verbose(message, this.getMetas());
        this.formatedLog('verbose', message);
    }
    overrideOptions(options) {
        this.logger.configure(options);
    }
    formatedLog(level, message, error, context) {
        let result = '';
        const color = chalk.default;
        const currentDate = new Date();
        const time = `${currentDate.getHours()}:${currentDate.getMinutes()}:${currentDate.getSeconds()}`;
        context = context ? context : this.context;
        switch (level) {
            case 'info':
                result = `[${color.blue('INFO')}] ${color.dim.yellow.bold.underline(time)} [${color.green(context)}]`;
                this.printMessage(result, message);
                break;
            case 'error':
                result = `[${color.red('ERR')}] ${color.dim.yellow.bold.underline(time)} [${color.green(this.context)}]`;
                this.printMessage(result, message);
                if (error && (process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'development')) {
                    console.log('Pretty error');
                    this.prettyError.render(error, true, true);
                }
                break;
            case 'warn':
                result = `[${color.yellow('WARN')}] ${color.dim.yellow.bold.underline(time)} [${color.green(context)}]`;
                this.printMessage(result, message);
                break;
            case 'verbose':
                result = `[${color.white('VERBOSE')}] ${color.dim.yellow.bold.underline(time)} [${color.green(context)}]`;
                this.printMessage(result, message);
                break;
            case 'debug':
                result = `[${color.white('DEBUG')}] ${color.dim.yellow.bold.underline(time)} [${color.green(context)}]`;
                this.printMessage(result, message);
                break;
            default:
                this.printMessage(result, message);
                break;
        }
    }
    printMessage(title, message) {
        if (typeof message === 'string') {
            console.log(`${title} ${message}`);
        }
        else {
            console.log(title, message);
        }
    }
};
CustomLoggerService = __decorate([
    common_1.Injectable()
    /**
     * Log
     * - to winston
     * - on console when on dev
     * - error on console printed with [pretty-error]()
     * - add the requestId of [express-http-context]() in the log file (not on console as it is probably useless or maybe just for prod console?)
     */
    ,
    __metadata("design:paramtypes", [Object])
], CustomLoggerService);
exports.CustomLoggerService = CustomLoggerService;
//# sourceMappingURL=logger.service.js.map