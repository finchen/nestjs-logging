"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const winston_constants_1 = require("./winston.constants");
const logger_service_1 = require("./logger/logger.service");
function createWinstonProviders(loggerOpts) {
    return [
        {
            provide: winston_constants_1.WINSTON_MODULE_PROVIDER,
            useFactory: () => {
                return new logger_service_1.CustomLoggerService(loggerOpts);
            },
        },
    ];
}
exports.createWinstonProviders = createWinstonProviders;
function createWinstonAsyncProviders(options) {
    return [
        {
            provide: winston_constants_1.WINSTON_MODULE_OPTIONS,
            useFactory: options.useFactory,
            inject: options.inject || [],
        },
        {
            provide: winston_constants_1.WINSTON_MODULE_PROVIDER,
            useFactory: (loggerOpts) => {
                return new logger_service_1.CustomLoggerService(loggerOpts);
            },
            inject: [winston_constants_1.WINSTON_MODULE_OPTIONS],
        },
    ];
}
exports.createWinstonAsyncProviders = createWinstonAsyncProviders;
//# sourceMappingURL=winston.providers.js.map