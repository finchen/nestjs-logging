"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AppError_1 = require("./AppError");
class RequestValidationError extends AppError_1.AppError {
    // tslint:disable-next-line:no-any
    constructor(message, fields) {
        // Providing default message and overriding status code.
        super(message || 'Request validation failed', 'operational');
        // Saving custom property.
        this.fields = fields || {};
    }
}
exports.RequestValidationError = RequestValidationError;
//# sourceMappingURL=RequestValidationError.js.map