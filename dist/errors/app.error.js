"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * usage: throw new AppError("Describe here what happened", true);
 */
class AppError extends Error {
    // All other unhandledError (e.g. trying to read undefined variable) refers to unknown code failures. Error Logging + alert
    /**
     *
     * @param message
     * @param isOperational `true` known cases, `false` unknown code failures
     */
    constructor(message, errorLevel) {
        // Calling parent constructor of base Error class.
        super(message);
        this.name = this.constructor.name;
        this.isOperational = errorLevel !== null && errorLevel === 'operational';
        this.isSystem = errorLevel !== null && errorLevel === 'system';
        this.isCritical = errorLevel !== null && errorLevel === 'critical';
        // Capturing stack trace, excluding constructor call from it.
        Error.captureStackTrace(this, this.constructor);
    }
    getResponse() {
        return {
            status: 500,
            name: this.name,
            message: this.message,
            isOperational: this.isOperational,
        };
    }
}
exports.AppError = AppError;
//# sourceMappingURL=app.error.js.map