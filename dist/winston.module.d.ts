import { DynamicModule } from '@nestjs/common';
import { WinstonModuleAsyncOptions, WinstonModuleOptions } from './winston.interfaces';
export declare class WinstonModule {
    static forRoot(options?: WinstonModuleOptions): DynamicModule;
    static forRootAsync(options: WinstonModuleAsyncOptions): DynamicModule;
}
