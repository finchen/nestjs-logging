import { NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
export declare class HttpRequestIdMiddleware implements NestMiddleware {
    headerName: string;
    use(req: Request, res: Response, next: NextFunction): void;
}
