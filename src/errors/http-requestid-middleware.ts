import { Injectable, NestMiddleware } from '@nestjs/common';
import { v4 } from 'uuid';
import * as httpContext from 'express-http-context';
import { Request, Response, NextFunction } from 'express';

@Injectable()
export class HttpRequestIdMiddleware implements NestMiddleware {
    headerName = 'X-RequestId';

    use(req: Request, res: Response, next: NextFunction) {
        // use a complete random uuid; v4
        const uuid = v4();
        httpContext.set('reqId', uuid);
        // Set it in header so it can be easily retrieved from client
        res.setHeader(this.headerName, uuid);
        next();
    }
}