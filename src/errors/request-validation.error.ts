import { AppError } from './app.error';

export class RequestValidationError extends AppError {
  // tslint:disable-next-line:no-any
  fields: any;

  // tslint:disable-next-line:no-any
  constructor(message?: string, fields?: any) {
    // Providing default message and overriding status code.
    super(message || 'Request validation failed', 'operational');

    // Saving custom property.
    this.fields = fields || {};
  }
}
