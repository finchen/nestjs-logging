import { ModuleMetadata } from '@nestjs/common/interfaces';
import { LoggerOptions } from 'winston';
import { RequestService } from './requests/request.service';

// export type WinstonModuleOptions = LoggerOptions;

export type WinstonModuleOptions = {
  context?: string;
} & Partial<LoggerOptions>;

export interface WinstonModuleAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  useFactory: (
    // tslint:disable-next-line:no-any
    ...args: any[]
  ) => Promise<WinstonModuleOptions> | WinstonModuleOptions;
  // tslint:disable-next-line:no-any
  inject?: any[];
}
