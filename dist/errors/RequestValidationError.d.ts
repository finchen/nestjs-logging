import { AppError } from './AppError';
export declare class RequestValidationError extends AppError {
    fields: any;
    constructor(message?: string, fields?: any);
}
