import { DynamicModule, Global, Module } from '@nestjs/common';
import {
  WinstonModuleAsyncOptions,
  WinstonModuleOptions,
} from './winston.interfaces';
import {
  createWinstonAsyncProviders,
  createWinstonProviders,
} from './winston.providers';
import { DefaultWinstonOptions } from './logger/default-winston-options';

@Global()
@Module({})
export class WinstonModule {
  static forRoot(options: WinstonModuleOptions = DefaultWinstonOptions.options()): DynamicModule {
    const providers = createWinstonProviders(options);
    return {
      module: WinstonModule,
      providers,
      exports: providers,
    };
  }

  static forRootAsync(options: WinstonModuleAsyncOptions): DynamicModule {
    const providers = createWinstonAsyncProviders(options);

    return {
      module: WinstonModule,
      imports: options.imports,
      providers,
      exports: providers,
    };
  }
}
