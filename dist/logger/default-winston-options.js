"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const winston = require("winston");
class DefaultWinstonOptions {
    /**
     * Config for winston logger
     * Please don't add a console logger as it is done in `LoggerService`
     */
    static options() {
        console.log('level: ' + process.env.LOG_LEVEL);
        let transports = [
            new winston.transports.File({
                filename: './logs/error.log',
                level: 'error',
                handleExceptions: true,
            }),
            new winston.transports.File({
                filename: './logs/combined.log',
                level: process.env.LOG_LEVEL,
            }),
        ];
        if (process.env.NODE_ENV === 'test') {
            transports = [
                new winston.transports.File({ filename: './logs/e2e.log' }),
            ];
        }
        const logger = {
            level: process.env.LOG_LEVEL,
            format: winston.format.json(),
            exitOnError: false,
            transports,
        };
        return logger;
    }
}
exports.DefaultWinstonOptions = DefaultWinstonOptions;
//# sourceMappingURL=default-winston-options.js.map