import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  HttpAdapterHost,
  Logger,
} from '@nestjs/common';
import { AppError } from './app.error';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  constructor(
    private readonly logger: Logger,
    private readonly host: HttpAdapterHost
  ) {
    console.log('AllExceptionsFilter constructor with http host');
  }

  catch(exception: HttpException | AppError | Error, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    const status = exception instanceof HttpException
      ? exception.getStatus()
      : HttpStatus.INTERNAL_SERVER_ERROR;

    let errorResponse: object = { status, name: 'Error' };
    let operationalError = false;

    if (exception instanceof HttpException) {
      const httpResponse = exception.getResponse();
      errorResponse =
        typeof httpResponse === 'string'
          ? { status: 500, message: httpResponse }
          : httpResponse;
    } else if (exception instanceof AppError) {
      errorResponse = exception.getResponse();
      if (exception.isOperational) {
        operationalError = true;
        const warnAt = exception.stack && exception.stack.split('\n').length > 1 ? exception.stack.split('\n')[1] : '';
        this.logger.warn(`Operational error caught: ${exception.getResponse().name} ${exception.getResponse().message} ${warnAt}`);
      }
    } else if (exception instanceof Error) {
      errorResponse = { status, name: 'Error', message: exception.message };
    }

    // Winston log (adds timestamp and other info)
    if( !operationalError ){
      this.logger.error(
        {
          ...errorResponse,
          request: request.url,
          method: request.method,
          // TODO add request.body but make sure not too big
        },
        exception.stack
      );
    }

    if (process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'development') {
      console.log('Error caught. Returning to the client -> ', errorResponse);
    }

    // As we are developing mainly an API we return json. We could return html if needed
    response.status(status).json(errorResponse);
  }
}
