import { ExceptionFilter, ArgumentsHost, HttpException, HttpAdapterHost, Logger } from '@nestjs/common';
import { AppError } from './app.error';
export declare class AllExceptionsFilter implements ExceptionFilter {
    private readonly logger;
    private readonly host;
    constructor(logger: Logger, host: HttpAdapterHost);
    catch(exception: HttpException | AppError | Error, host: ArgumentsHost): void;
}
