import { ApiModelProperty } from '@nestjs/swagger';
import { NgxLoggerLevel } from '../ngx-logger-level';

export class AddLogPayload {
  @ApiModelProperty({
    required: true,
  })
  level: NgxLoggerLevel | undefined;
  @ApiModelProperty({
    required: true,
  })
  timestamp: string | undefined;
  @ApiModelProperty({
    required: true,
  })
  fileName: string | undefined;
  @ApiModelProperty({
    required: true,
  })
  lineNumber: string | undefined;
  @ApiModelProperty({
    required: true,
  })
  message: string | undefined;
  @ApiModelProperty({
    required: true,
  })
  // tslint:disable-next-line: no-any
  additional: any[] | undefined;
}
