"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var WinstonModule_1;
"use strict";
const common_1 = require("@nestjs/common");
const winston_providers_1 = require("./winston.providers");
const default_winston_options_1 = require("./logger/default-winston-options");
let WinstonModule = WinstonModule_1 = class WinstonModule {
    static forRoot(options = default_winston_options_1.DefaultWinstonOptions.options()) {
        const providers = winston_providers_1.createWinstonProviders(options);
        return {
            module: WinstonModule_1,
            providers,
            exports: providers,
        };
    }
    static forRootAsync(options) {
        const providers = winston_providers_1.createWinstonAsyncProviders(options);
        return {
            module: WinstonModule_1,
            imports: options.imports,
            providers,
            exports: providers,
        };
    }
};
WinstonModule = WinstonModule_1 = __decorate([
    common_1.Global(),
    common_1.Module({})
], WinstonModule);
exports.WinstonModule = WinstonModule;
//# sourceMappingURL=winston.module.js.map