"use strict";
/* tslint:disable:only-arrow-functions max-classes-per-file */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const testing_1 = require("@nestjs/testing");
const chai_1 = require("chai");
const winston_constants_1 = require("./winston.constants");
const winston_module_1 = require("./winston.module");
describe('Winston module', function () {
    it('boots successfully', async function () {
        const rootModule = await testing_1.Test.createTestingModule({
            imports: [winston_module_1.WinstonModule.forRoot({})],
        }).compile();
        chai_1.expect(rootModule.get(winston_constants_1.WINSTON_MODULE_PROVIDER)).to.be.an('object');
    });
    it('boots successfully asynchronously', async function () {
        let ConfigService = class ConfigService {
            constructor() {
                this.loggerOptions = {};
            }
        };
        ConfigService = __decorate([
            common_1.Injectable()
        ], ConfigService);
        let FeatureModule = class FeatureModule {
        };
        FeatureModule = __decorate([
            common_1.Module({
                providers: [ConfigService],
                exports: [ConfigService],
            })
        ], FeatureModule);
        const rootModule = await testing_1.Test.createTestingModule({
            imports: [
                winston_module_1.WinstonModule.forRootAsync({
                    imports: [FeatureModule],
                    useFactory: (cfg) => cfg.loggerOptions,
                    inject: [ConfigService],
                }),
            ],
        }).compile();
        const app = rootModule.createNestApplication();
        await app.init();
        chai_1.expect(rootModule.get(winston_constants_1.WINSTON_MODULE_PROVIDER)).to.be.an('object');
    });
});
//# sourceMappingURL=winston.module.spec.js.map