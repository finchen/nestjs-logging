import { LoggerOptions } from 'winston';
export declare class DefaultWinstonOptions {
    /**
     * Config for winston logger
     * Please don't add a console logger as it is done in `LoggerService`
     */
    static options(): LoggerOptions;
}
