import { AppError } from './app.error';

export class EmailTakenError extends AppError {
  constructor(message: string) {
    // Providing default message and overriding status code.
    super(message || 'Specified E-Mail is already taken', 'operational');
  }
}
