"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const app_error_1 = require("./app.error");
let AllExceptionsFilter = class AllExceptionsFilter {
    constructor(logger, host) {
        this.logger = logger;
        this.host = host;
        console.log('AllExceptionsFilter constructor with http host');
    }
    catch(exception, host) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();
        const status = exception instanceof common_1.HttpException
            ? exception.getStatus()
            : common_1.HttpStatus.INTERNAL_SERVER_ERROR;
        let errorResponse = { status, name: 'Error' };
        let operationalError = false;
        if (exception instanceof common_1.HttpException) {
            const httpResponse = exception.getResponse();
            errorResponse =
                typeof httpResponse === 'string'
                    ? { status: 500, message: httpResponse }
                    : httpResponse;
        }
        else if (exception instanceof app_error_1.AppError) {
            errorResponse = exception.getResponse();
            if (exception.isOperational) {
                operationalError = true;
                const warnAt = exception.stack && exception.stack.split('\n').length > 1 ? exception.stack.split('\n')[1] : '';
                this.logger.warn(`Operational error caught: ${exception.getResponse().name} ${exception.getResponse().message} ${warnAt}`);
            }
        }
        else if (exception instanceof Error) {
            errorResponse = { status, name: 'Error', message: exception.message };
        }
        // Winston log (adds timestamp and other info)
        if (!operationalError) {
            this.logger.error(Object.assign({}, errorResponse, { request: request.url, method: request.method }), exception.stack);
        }
        if (process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'development') {
            console.log('Error caught. Returning to the client -> ', errorResponse);
        }
        // As we are developing mainly an API we return json. We could return html if needed
        response.status(status).json(errorResponse);
    }
};
AllExceptionsFilter = __decorate([
    common_1.Catch(),
    __metadata("design:paramtypes", [common_1.Logger, Object])
], AllExceptionsFilter);
exports.AllExceptionsFilter = AllExceptionsFilter;
//# sourceMappingURL=allexceptions.filter.js.map