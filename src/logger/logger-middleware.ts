import { Injectable, NestMiddleware, Inject, Logger } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  constructor(
    @Inject('winston')
    private readonly logger: Logger
  ) {}

  
  use(req: Request, res: Response, next: Function) {
    this.logger.debug(`[${req.method}] ${req.originalUrl}`);
    next();
  }
}
