import * as winston from 'winston';
import {
  FileTransportInstance,
  ConsoleTransportInstance,
} from 'winston/lib/winston/transports';
import { LoggerOptions } from 'winston';

export class DefaultWinstonOptions {
  /**
   * Config for winston logger
   * Please don't add a console logger as it is done in `LoggerService`
   */
  static options(): LoggerOptions {
    console.log('level: ' + process.env.LOG_LEVEL);


    let transports: Array<FileTransportInstance | ConsoleTransportInstance> = [
      new winston.transports.File({
        filename: './logs/error.log',
        level: 'error',
        handleExceptions: true,
      }),
      new winston.transports.File({
        filename: './logs/combined.log',
        level: process.env.LOG_LEVEL,
      }),
    ];

    if (process.env.NODE_ENV === 'test') {
      transports = [
        new winston.transports.File({ filename: './logs/e2e.log' }),
      ];
    }

    const logger = {
      level: process.env.LOG_LEVEL,
      format: winston.format.json(),
      exitOnError: false,
      transports,
    };

    return logger;
  }
}
