import { NgxLoggerLevel } from '../ngx-logger-level';
export declare class AddLogPayload {
    level: NgxLoggerLevel | undefined;
    timestamp: string | undefined;
    fileName: string | undefined;
    lineNumber: string | undefined;
    message: string | undefined;
    additional: any[] | undefined;
}
