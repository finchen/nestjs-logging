import { Injectable, NestMiddleware } from '@nestjs/common';
import * as httpContext from 'express-http-context';
import { Request, Response, NextFunction } from 'express';

@Injectable()
export class HttpContextMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: NextFunction) {
        httpContext.middleware(req, res, next);
    }
}