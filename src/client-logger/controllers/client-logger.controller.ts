import { Controller, Post, Body, Inject, Logger } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { AddLogPayload } from './payloads/add-log.payload';
import { NgxLoggerLevel } from './ngx-logger-level';

@Controller('client-logger')
@ApiUseTags('client-logger')
export class ClientLoggerController {
  constructor(
    @Inject('winston')
    private readonly logger: Logger
  ) {}

  @Post('add')
  add(@Body() payload: AddLogPayload) {
    switch (payload.level) {
      case NgxLoggerLevel.FATAL:
      case NgxLoggerLevel.ERROR:
        this.logger.error(payload, undefined, 'Client-Side');
        break;
      case NgxLoggerLevel.WARN:
        this.logger.warn(payload, 'Client-Side');
        break;
      case NgxLoggerLevel.LOG:
        this.logger.log(payload, 'Client-Side');
        break;
      case NgxLoggerLevel.INFO:
      case NgxLoggerLevel.TRACE:
      case NgxLoggerLevel.DEBUG:
      default:
        this.logger.debug(payload, 'Client-Side');
        break;
    }
  }
}
