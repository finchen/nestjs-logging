import { AppError } from './app.error';
export declare class EmailTakenError extends AppError {
    constructor(message: string);
}
