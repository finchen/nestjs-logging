"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NgxLoggerLevel;
(function (NgxLoggerLevel) {
    NgxLoggerLevel[NgxLoggerLevel["TRACE"] = 0] = "TRACE";
    NgxLoggerLevel[NgxLoggerLevel["DEBUG"] = 1] = "DEBUG";
    NgxLoggerLevel[NgxLoggerLevel["INFO"] = 2] = "INFO";
    NgxLoggerLevel[NgxLoggerLevel["LOG"] = 3] = "LOG";
    NgxLoggerLevel[NgxLoggerLevel["WARN"] = 4] = "WARN";
    NgxLoggerLevel[NgxLoggerLevel["ERROR"] = 5] = "ERROR";
    NgxLoggerLevel[NgxLoggerLevel["FATAL"] = 6] = "FATAL";
    NgxLoggerLevel[NgxLoggerLevel["OFF"] = 7] = "OFF";
})(NgxLoggerLevel = exports.NgxLoggerLevel || (exports.NgxLoggerLevel = {}));
//# sourceMappingURL=ngx-logger-level.js.map