import { Provider } from '@nestjs/common';
import {
  WINSTON_MODULE_OPTIONS,
  WINSTON_MODULE_PROVIDER,
} from './winston.constants';
import {
  WinstonModuleAsyncOptions,
  WinstonModuleOptions,
} from './winston.interfaces';
import { CustomLoggerService } from './logger/logger.service';

export function createWinstonProviders(
  loggerOpts: WinstonModuleOptions
): Provider[] {
  return [
    {
      provide: WINSTON_MODULE_PROVIDER,
      useFactory: () => {
        return new CustomLoggerService(loggerOpts);
      },
    },
  ];
}

export function createWinstonAsyncProviders(
  options: WinstonModuleAsyncOptions
): Provider[] {
  return [
    {
      provide: WINSTON_MODULE_OPTIONS,
      useFactory: options.useFactory,
      inject: options.inject || [],
    },
    {
      provide: WINSTON_MODULE_PROVIDER,
      useFactory: (loggerOpts: WinstonModuleOptions) => {
        return new CustomLoggerService(loggerOpts);
      },
      inject: [WINSTON_MODULE_OPTIONS],
    },
  ];
}
