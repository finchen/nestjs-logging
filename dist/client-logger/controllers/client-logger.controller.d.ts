import { Logger } from '@nestjs/common';
import { AddLogPayload } from './payloads/add-log.payload';
export declare class ClientLoggerController {
    private readonly logger;
    constructor(logger: Logger);
    add(payload: AddLogPayload): void;
}
