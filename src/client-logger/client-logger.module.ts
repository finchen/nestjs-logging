import { Module } from '@nestjs/common';
import { ClientLoggerController } from './controllers/client-logger.controller';

@Module({
  controllers: [ClientLoggerController],
})
export class ClientLoggerModule {}
