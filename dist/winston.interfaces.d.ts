import { ModuleMetadata } from '@nestjs/common/interfaces';
import { LoggerOptions } from 'winston';
export declare type WinstonModuleOptions = {
    context?: string;
} & Partial<LoggerOptions>;
export interface WinstonModuleAsyncOptions extends Pick<ModuleMetadata, 'imports'> {
    useFactory: (...args: any[]) => Promise<WinstonModuleOptions> | WinstonModuleOptions;
    inject?: any[];
}
