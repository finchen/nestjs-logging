import { Module, Global, Scope } from '@nestjs/common';
import { RequestService } from './request.service';

@Global()
@Module({
  providers: [
    {
      provide: RequestService,
      useClass: RequestService,
      scope: Scope.REQUEST,
    },
  ],
  exports: [RequestService],
})
export class RequestModule {}
