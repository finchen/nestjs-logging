"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_error_1 = require("./app.error");
class RequestValidationError extends app_error_1.AppError {
    // tslint:disable-next-line:no-any
    constructor(message, fields) {
        // Providing default message and overriding status code.
        super(message || 'Request validation failed', 'operational');
        // Saving custom property.
        this.fields = fields || {};
    }
}
exports.RequestValidationError = RequestValidationError;
//# sourceMappingURL=request-validation.error.js.map