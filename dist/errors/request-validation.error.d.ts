import { AppError } from './app.error';
export declare class RequestValidationError extends AppError {
    fields: any;
    constructor(message?: string, fields?: any);
}
