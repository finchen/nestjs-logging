"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AppError_1 = require("./AppError");
class EmailTakenError extends AppError_1.AppError {
    constructor(message) {
        // Providing default message and overriding status code.
        super(message || 'Specified E-Mail is already taken', 'operational');
    }
}
exports.EmailTakenError = EmailTakenError;
//# sourceMappingURL=EmailTakenError.js.map