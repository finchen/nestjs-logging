import { ExceptionFilter, HttpException, ArgumentsHost, Logger } from '@nestjs/common';
import { AppError } from './AppError';
/**
 * Catch any exception
 * - log
 * - if one of the http (https://docs.nestjs.com/exception-filters)
 * - if not in prod: verbose
 *
 */
export declare class ErrorFilter implements ExceptionFilter {
    private readonly logger;
    constructor(logger: Logger);
    catch(exception: HttpException | AppError | Error, host: ArgumentsHost): void;
}
