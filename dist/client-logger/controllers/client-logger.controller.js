"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const add_log_payload_1 = require("./payloads/add-log.payload");
const ngx_logger_level_1 = require("./ngx-logger-level");
let ClientLoggerController = class ClientLoggerController {
    constructor(logger) {
        this.logger = logger;
    }
    add(payload) {
        switch (payload.level) {
            case ngx_logger_level_1.NgxLoggerLevel.FATAL:
            case ngx_logger_level_1.NgxLoggerLevel.ERROR:
                this.logger.error(payload, undefined, 'Client-Side');
                break;
            case ngx_logger_level_1.NgxLoggerLevel.WARN:
                this.logger.warn(payload, 'Client-Side');
                break;
            case ngx_logger_level_1.NgxLoggerLevel.LOG:
                this.logger.log(payload, 'Client-Side');
                break;
            case ngx_logger_level_1.NgxLoggerLevel.INFO:
            case ngx_logger_level_1.NgxLoggerLevel.TRACE:
            case ngx_logger_level_1.NgxLoggerLevel.DEBUG:
            default:
                this.logger.debug(payload, 'Client-Side');
                break;
        }
    }
};
__decorate([
    common_1.Post('add'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [add_log_payload_1.AddLogPayload]),
    __metadata("design:returntype", void 0)
], ClientLoggerController.prototype, "add", null);
ClientLoggerController = __decorate([
    common_1.Controller('client-logger'),
    swagger_1.ApiUseTags('client-logger'),
    __param(0, common_1.Inject('winston')),
    __metadata("design:paramtypes", [common_1.Logger])
], ClientLoggerController);
exports.ClientLoggerController = ClientLoggerController;
//# sourceMappingURL=client-logger.controller.js.map