import * as winston from 'winston';
import * as chalk from 'chalk';
import * as PrettyError from 'pretty-error';
import { LoggerOptions, Logger } from 'winston';

import { Injectable, LoggerService } from '@nestjs/common';
import { WinstonModuleOptions } from '../winston.interfaces';
import * as httpContext from 'express-http-context';
import { DefaultWinstonOptions } from './default-winston-options';

@Injectable()
/**
 * Log
 * - to winston
 * - on console when on dev 
 * - error on console printed with [pretty-error]()
 * - add the requestId of [express-http-context]() in the log file (not on console as it is probably useless or maybe just for prod console?)
 */
export class CustomLoggerService implements LoggerService {
  private readonly logger: Logger;
  private prettyError: PrettyError;
  /**
   * global/app context
   * does not apply for class or filename
   * default `Main`
   */
  private context: string;

  constructor(private readonly config: WinstonModuleOptions) {
    this.logger = winston.createLogger(config); 
    this.prettyError = new PrettyError();
    this.prettyError.skipNodeFiles();
    //this.prettyError.skipPackage('express', '@nestjs/common', '@nestjs/core');
    this.context = this.config.context ? this.config.context : 'Main';
  }

  setContext(c: string) {
    this.context = c;
  }

  getMetas(context?: string) {
    const currentDate = new Date();

    const extras = {
      timestamp: currentDate.toISOString(),
      context: context ? context : this.context,
      requestId: httpContext.get('reqId'),
    };

    return extras;
  }

  log(message: string): void {
    this.logger.info(message, this.getMetas());
    this.formatedLog('info', message);
  }

  error(message: string | object, trace?: string, context?: string): void {
    if (typeof message === 'string') {
      this.logger.error(
        `${message} -> (${trace || 'trace not provided !'})`,
        this.getMetas(context)
      );
    } else {
      this.logger.error(`Error -> (${trace || 'trace not provided !'})`, {
        ...message, // should we set it on key `response` ?
        ...this.getMetas(context),
      });
    }
    this.formatedLog('error', message, trace, context);
  }

  warn(message: string): void {
    this.logger.warn(message, this.getMetas());
    this.formatedLog('warn', message);
  }

  debug(message: string): void {
    this.logger.debug(message, this.getMetas());
    this.formatedLog('debug', message);
  }

  verbose(message: string): void {
    this.logger.verbose(message, this.getMetas());
    this.formatedLog('verbose', message);
  }

  overrideOptions(options: LoggerOptions) {
    this.logger.configure(options);
  }

  private formatedLog(
    level: string,
    message: string | object,
    error?: string,
    context?: string
  ): void {
    let result = '';
    const color = chalk.default;
    const currentDate = new Date();
    const time = `${currentDate.getHours()}:${currentDate.getMinutes()}:${currentDate.getSeconds()}`;
    context = context ? context : this.context;

    switch (level) {
      case 'info':
        result = `[${color.blue('INFO')}] ${color.dim.yellow.bold.underline(
          time
        )} [${color.green(context)}]`;
        this.printMessage(result, message);
        break;
      case 'error':
          result = `[${color.red('ERR')}] ${color.dim.yellow.bold.underline(
            time
          )} [${color.green(this.context)}]`;
          this.printMessage(result, message);
        if (error && (process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'development')) {
          console.log('Pretty error');
          this.prettyError.render(error, true, true);
        }
        break;
      case 'warn':
        result = `[${color.yellow('WARN')}] ${color.dim.yellow.bold.underline(
          time
        )} [${color.green(context)}]`;
        this.printMessage(result, message);
        break;
      case 'verbose':
        result = `[${color.white('VERBOSE')}] ${color.dim.yellow.bold.underline(
          time
        )} [${color.green(context)}]`;
        this.printMessage(result, message);
        break;
      case 'debug':
        result = `[${color.white('DEBUG')}] ${color.dim.yellow.bold.underline(
          time
        )} [${color.green(context)}]`;
        this.printMessage(result, message);
        break;
      default:
        this.printMessage(result, message);
        break;
    }
  }

  private printMessage(title: string, message: string | object){
    if (typeof message === 'string') {
      console.log(`${title} ${message}`);
    }else{
      console.log(title, message);
    }
  }
}
